﻿using Homework13.FigureVisitor;
using Homework13.Visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework13.SerializeStategy
{
    public class SerializeContext
    {
        ISerializeStrategy _strategy;
        IVisitor _visitor;
        public string Serialize(Request data)
        {
            SetStrategy(data);
            SetVisitor(data);
            return _strategy.Execute(_visitor, data);
        }

        private void SetVisitor(Request data)
        {
            _visitor = data.Visitor;
        }

        private void SetStrategy(Request data)
        {
            _strategy = data.Strategy;
        }
    }
}
