﻿using Homework13.FigureVisitor;
using Homework13.Visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework13.SerializeStategy
{
    public class Request
    {
        public ISerializeStrategy Strategy { get; }
        public IVisitor Visitor { get; }
        public IFigure Data { get; }

        public Request(ISerializeStrategy strategy, IVisitor visitor, IFigure data)
        {
            Strategy = strategy;
            Visitor = visitor;
            Data = data;
        }
    }
}
