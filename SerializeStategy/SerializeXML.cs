﻿using Homework13.FigureVisitor;
using Homework13.Visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework13.SerializeStategy
{
    public class SerializeXML : ISerializeStrategy
    {
        public IFigure Data { get; set; }
        public string Execute(IVisitor visitor, Request request)
        {
            Data = request.Data;
            return visitor.Visit(this);
        }
    }
}
