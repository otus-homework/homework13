﻿using Homework13.SerializeStategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework13.FigureVisitor
{
    public class Square : IFigure
    {
        public Point Point1 { get; set; }
        public Point Point2 { get; set; }

        public Square(Point point1, Point point2)
        {
            Point1 = point1;
            Point2 = point2;
        }
    }
}
