﻿using Homework13.SerializeStategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework13.FigureVisitor
{
    public class Circle : IFigure
    {
        public Point Point { get; }
        public int Radius { get; }

        public Circle(Point point, int radius)
        {
            Point = point;
            Radius = radius;
        }
    }
}
