﻿using Homework13.FigureVisitor;
using Homework13.SerializeStategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework13.Visitor
{
    class PointVisitor: IVisitor
    {
        public string Visit(SerializeXML serialize)
        {
            Point point = (Point)serialize.Data;

            StringBuilder xml = new StringBuilder();
            xml.AppendLine("XML:");
            xml.AppendLine("<? xml version = \"1.0\" encoding = \"utf - 8\" ?>");
            xml.AppendLine($"<{nameof(Point)}>");
            xml.AppendLine($@"   <X>{point.X}</X>");
            xml.AppendLine($@"   <Y>{point.Y}</Y>");
            xml.Append($"</{nameof(Point)}>");
            return xml.ToString();
        }

        public string Visit(SerializeJSON serialize)
        {
            Point point = (Point)serialize.Data;

            StringBuilder json = new StringBuilder();
            json.AppendLine("JSON:");
            json.AppendLine("{");
            json.AppendLine($@"   figure: {nameof(Point)},");
            json.AppendLine($@"   x: {point.X},");
            json.AppendLine($@"   y: {point.Y}");
            json.Append("}");
            return json.ToString();
        }
    }
}
