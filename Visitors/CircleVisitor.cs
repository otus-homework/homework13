﻿using Homework13.FigureVisitor;
using Homework13.SerializeStategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework13.Visitor
{
    public class CircleVisitor: IVisitor
    {
        public string Visit(SerializeXML serialize)
        {
            Circle circle = (Circle)serialize.Data;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("XML:");
            sb.AppendLine("<? xml version = \"1.0\" encoding = \"utf - 8\" ?>");
            sb.AppendLine($"<{nameof(Circle)}>");
            sb.AppendLine($"   <{nameof(Point)}>");

            //Отрисовка точки без xml заголовков
            var context = new SerializeContext();
            var xmlPoint = context.Serialize(new Request(serialize, new PointVisitor(), circle.Point)).Split('\n');
            AppendPointSerializeData(sb, xmlPoint, "   ", 3, -1);

            sb.AppendLine($"   </{nameof(Point)}>");
            sb.AppendLine($@"   <Radius>{circle.Radius}</radius>");
            sb.Append($"</{nameof(Circle)}>");
            return sb.ToString();
        }

        public string Visit(SerializeJSON serialize)
        {
            Circle circle = (Circle)serialize.Data;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("JSON:");
            sb.AppendLine("{");
            sb.AppendLine($@"   figure: {nameof(Circle)},");
            sb.AppendLine($@"   point:");

            //Отрисовка точки без json заголовков
            var context = new SerializeContext();
            var jsonPoint = context.Serialize(new Request(serialize, new PointVisitor(), circle.Point)).Split('\n');
            AppendPointSerializeData(sb, jsonPoint, "\t", 1);

            sb.AppendLine($@"   radius: {circle.Radius}");
            sb.Append("}");
            return sb.ToString();
        }

        private static void AppendPointSerializeData(StringBuilder sb, string[] data, string space, int beginPos, int endPos=0)
        {
            for (int i = beginPos; i < data.Length + endPos; i++)
            {
                sb.AppendLine(space + data[i]);
            }
        }
    }
}
