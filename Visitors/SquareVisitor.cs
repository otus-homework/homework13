﻿using Homework13.FigureVisitor;
using Homework13.SerializeStategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework13.Visitor
{
    public class SquareVisitor:IVisitor
    {
        public string Visit(SerializeXML serialize)
        {
            Square square = (Square)serialize.Data;
            var context = new SerializeContext();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("XML:");
            sb.AppendLine("<? xml version = \"1.0\" encoding = \"utf - 8\" ?>");
            sb.AppendLine($"<{nameof(Square)}>");

            //Отрисовка первой точки без xml заголовков
            sb.AppendLine($"   <{nameof(Point)}1>");
            var xmlPoint1 = context.Serialize(new Request(serialize, new PointVisitor(), square.Point1)).Split('\n');
            AppendPointSerializeData(sb, xmlPoint1, "   ", 3, -1);
            sb.AppendLine($"   </{nameof(Point)}1>");

            //Отрисовка второй точки без xml заголовков
            sb.AppendLine($"   <{nameof(Point)}2>");
            var xmlPoint2 = context.Serialize(new Request(serialize, new PointVisitor(), square.Point2)).Split('\n');
            AppendPointSerializeData(sb, xmlPoint2, "   ", 3, -1);
            sb.AppendLine($"   </{nameof(Point)}2>");

            sb.Append($"</{nameof(Square)}>");
            return sb.ToString();
        }

        public string Visit(SerializeJSON serialize)
        {
            Square square = (Square)serialize.Data;
            var context = new SerializeContext();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("JSON:");
            sb.AppendLine("{");
            sb.AppendLine($@"   figure: {nameof(Square)},");

            //Отрисовка первой точки без json заголовков
            sb.AppendLine($@"   point1:");
            var jsonPoint1 = context.Serialize(new Request(serialize, new PointVisitor(), square.Point1)).Split('\n');
            AppendPointSerializeData(sb, jsonPoint1, "\t", 1);

            //Отрисовка второй точки без json заголовков
            sb.AppendLine($@"   point2:");
            var jsonPoint2 = context.Serialize(new Request(serialize, new PointVisitor(), square.Point2)).Split('\n');
            AppendPointSerializeData(sb, jsonPoint2, "\t", 1);

            sb.Append("}");
            return sb.ToString();
        }

        private static void AppendPointSerializeData(StringBuilder sb, string[] data, string space, int beginPos, int endPos = 0)
        {
            for (int i = beginPos; i < data.Length + endPos; i++)
            {
                sb.AppendLine(space + data[i]);
            }
        }
    }
}
