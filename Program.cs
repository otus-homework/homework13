﻿using Homework13.FigureVisitor;
using Homework13.SerializeStategy;
using Homework13.Visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework13
{
    class Program
    {
        static void Main(string[] args)
        {
            SerializeContext context = new SerializeContext();
            var xml = new SerializeXML();
            var json = new SerializeJSON();

            var point = new Point(10, 10);
            var pointVisitor = new PointVisitor();
            Console.WriteLine(context.Serialize(new Request(xml, pointVisitor, point)));
            Console.WriteLine(context.Serialize(new Request(json, pointVisitor, point)));
            Console.WriteLine();

            var circle = new Circle(new Point(1, 1), 3);
            var circleVisitor = new CircleVisitor();
            Console.WriteLine(context.Serialize(new Request(xml, circleVisitor, circle)));
            Console.WriteLine(context.Serialize(new Request(json, circleVisitor, circle)));
            Console.WriteLine();

            var square = new Square(new Point(3, 3), new Point(5, 5));
            var squareVisitor = new SquareVisitor();
            Console.WriteLine(context.Serialize(new Request(xml, squareVisitor, square)));
            Console.WriteLine(context.Serialize(new Request(json, squareVisitor, square)));
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
